using System.Collections.Generic;

using JMD.DataBase;
using JMD.Token;
using JMD.Token.Animator;


namespace JMD.Token.Database
{
  class DatabaseConnector : IDatabaseConector<ITokenInfo, ITokenCarousellElement>
  {
    public List<ITokenCarousellElement> getTokenAnimations()
    {
      List<ITokenCarousellElement> lAnimation = new List<ITokenCarousellElement>();
      //******************* */ mock del listado**********************
      // hay que cargar fichero local?
      List<string> lcategories = new List<string>() { "services", "activities" };
      foreach (string cat in lcategories)
      {
        // generamos elemento de animación
        ITokenCarousellElement tce = new TokenCarousellElement();
        tce.time = 4.0f;
        tce.type = eTokenAnimation.ICON;
        tce.category = cat;


        lAnimation.Add(tce);


      }
      lAnimation[1].type = eTokenAnimation.ID;
      return lAnimation;
      /** fin mock listado animaciones */
    }

    public List<ITokenInfo> getTokens(List<ITokenInfo> lFilter)
    {
      return mockGetTokens();
    }

    public bool init()
    {
      throw new System.NotImplementedException();
    }

    public void saveTokens(List<ITokenInfo> ltoken)
    {
      throw new System.NotImplementedException();
    }

    private List<ITokenInfo> mockGetTokens()
    {

      List<ITokenInfo> lTI = new List<ITokenInfo>();

      // asignamos valores
      ITokenInfo ti = new TokenInfo();
      ti.Id = "services1";
      ti.Category = "services";
      ti.Title = new List<string>() { "Recicle Point", "Punto de reciclaje" };
      ti.Subtitle = new List<string>() { "Glass, paper, oil", "Cristal, papel, aceite" };
      ti.Video = new List<string>() { "Video/test_tall.mp4" };
      ti.Icon = "garbage";
      ti.Reference = "Reciclar 1";
      lTI.Add(ti);

      ti = new TokenInfo();
      ti.Category = "services";
      ti.Id = "services2";
      ti.Title = new List<string>() { "Recicle Point 2", "Punto de reciclaje 2" };
      ti.Subtitle = new List<string>() { "Grey water", "Aguas grises" };
      ti.Icon = "garbage";
      ti.Reference = "Reciclar 2";
      lTI.Add(ti);

      // papeleras
      for (int i = 1; i <= 11; i++)
      {
        ti = new TokenInfo();
        ti.Id = "servicesGarbage" + i;
        ti.Category = "services";
        ti.Title = new List<string>() { "Papelera" };
        ti.Subtitle = new List<string>() { "Grey water", "Aguas grises" };
        ti.Icon = "garbage-2";
        ti.Reference = "Reciclar 1 (" + i + ")";
        lTI.Add(ti);
      }

      // extintores
      for (int i = 0; i <= 12; i++)
      {
        ti = new TokenInfo();
        ti.Id = "FirePrevention" + i;
        ti.Category = "services";
        ti.Title = new List<string>() { "" };
        ti.Subtitle = new List<string>() { "" };
        ti.Icon = "fire-extinguisher";
        ti.Reference = "Fire prevention (" + i + ")";
        lTI.Add(ti);
      }

      // Parking
      for (int i = 0; i <= 4; i++)
      {
        ti = new TokenInfo();
        ti.Id = "Parking" + i;
        ti.Category = "services";
        ti.Title = new List<string>() { "Parking" };
        ti.Subtitle = new List<string>() { "" };
        ti.Icon = "parking";
        ti.Reference = "Parking (" + i + ")";
        lTI.Add(ti);
      }
      // Activities
      for (int i = 0; i <= 10; i++)
      {
        ti = new TokenInfo();
        ti.Id = "Parking" + i;
        ti.Category = "activities";
        ti.Title = new List<string>() { "Texto " + i };
        ti.Subtitle = new List<string>() { "" };
        // ti.Icon = "parking";
        ti.Reference = "CamaraToken";
        lTI.Add(ti);
      }


      return lTI;
    }
  }
}