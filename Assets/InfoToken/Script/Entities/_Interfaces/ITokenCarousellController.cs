using System.Collections.Generic;
using JMD.Token;
using JMD.Token.Controller;

namespace JMD.Token.Animator
{
  interface ITokenCarousellController
  {
    // get order to generate animation
    void CarousellInitTokens(ITokenController tokenController);

    // recupera la configuració que indica l'ordre del carousell
    void CarousellgetOrder();

    // inicia la animación
    void CarousellStart();

    void CarousellNext();

  }
}