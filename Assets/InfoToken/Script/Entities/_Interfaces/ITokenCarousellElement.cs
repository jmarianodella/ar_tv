using System.Collections.Generic;
using JMD.Token;

namespace JMD.Token.Animator
{
  public interface ITokenCarousellElement
  {
    eTokenAnimation type { get; set; }
    float time { get; set; }
    string category { get; set; }
   List<List<ITokenAnimator>> lTokenAnimator {get; set;}
  }
}