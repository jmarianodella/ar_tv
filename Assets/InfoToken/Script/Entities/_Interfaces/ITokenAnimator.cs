using System.Collections.Generic;
using JMD.Token;

namespace JMD.Token.Animator
{

  public interface ITokenAnimator
  {
    // init token
    void InitTokenAnimation();
    // despierta al token
    void TokenAwake(bool awake);

    // inicia/para el video
    void TokenPlayVideo(bool play);

    // animación cuando el token está seleccionado
    void TokenSelected(bool selected);
    
  }
    
}