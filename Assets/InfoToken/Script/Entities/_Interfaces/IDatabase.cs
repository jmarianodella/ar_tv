using System.Collections.Generic;
using System;

namespace JMD.DataBase
{
    public interface IDatabaseConector <T, G>
    {
      // intenta establecer con la BDD,
      //  si tiene exito retorna true, falso en caso contario
      bool init();

      // recupera los tokens guardados
      List<T> getTokens(List<T> lFilter);      


      // guarda el listado de tokens
      void saveTokens(List<T> ltoken);

      // recupera el listado de las animaciones
      List<G> getTokenAnimations();

    }
}