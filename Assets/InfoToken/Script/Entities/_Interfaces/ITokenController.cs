using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using JMD.Token;

namespace JMD.Token.Controller
{
  public interface ITokenController
  {
    // distribuye los tokens entre categorias
    void SortTokens(List<ITokenInfo> ltoken);

    // lista las categorias de los tokens guardados
    List<string> GetKeys();

    // devuelve los tokens de una categoría en concreto
    List<ITokenInfo> GetTokens(string category);
    
    // diccionario de los gameObjects asociados a los tokens
    void AddGameObject(string idToken, GameObject gameObject);
    GameObject GetGameObject(string idToken);

  }

}