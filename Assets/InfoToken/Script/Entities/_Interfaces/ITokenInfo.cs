using System.Collections.Generic;
using System;

namespace JMD.Token
{
  public interface ITokenInfo
  {
    // identificador del elemento
    string Id { get; set; }

    // desde cuando el token está activo
    DateTime Since { get; set; }

    // desde cuando el token está activo
    DateTime Until { get; set; }

    // objeto al cual está asociado
    string Reference { get; set; }

    // lista de títulos a mostrar.
    //  Debe tener la misma logintud que subtítulo
    List<string> Title { get; set; }

    // lista de subtitulos a mostrar
    List<string> Subtitle { get; set; }

    // lista de vídeos a mostrar
    List<string> Video { get; set; }

    // icono sobre el mapa
    string Icon { get; set; }

    string Category {get; set;}

  }

}