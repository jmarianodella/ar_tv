using System.Collections.Generic;
using JMD.Token;

namespace JMD.Token.Animator
{

  public interface ITokenCarousell
  {
    // inicializa el listado de tokens para el carousell
    void InitTokens(List<ITokenInfo> ltokens);

    // tiempo mínimo de animación
    float MinAnimationTime { set; get; }

    // indica por qué campo se agruparán las animaciónes
    void AnimationBy(eTokenAnimation type);

    void StartTokenCarousell();

    
  }

}