using System.Collections.Generic;
using JMD.Token;

using UnityEngine;

namespace JMD.Token.Controller
{
  public class TokenController : ITokenController
  {


  
    private Dictionary<string, List<ITokenInfo>> dic =
        new Dictionary<string, List<ITokenInfo>>();

    private Dictionary<string, GameObject> dGameObjects =
        new Dictionary<string, GameObject>();

    public void AddGameObject(string idToken, GameObject gameObject)
    {
      dGameObjects[idToken] = gameObject;
    }

    public GameObject GetGameObject(string idToken)
    {
      return dGameObjects[idToken];
    }

    // devuelve el listado de claves
    public List<string> GetKeys()
    {
      List<string> lkeys = new List<string>();
      foreach (string item in dic.Keys)
      {
        lkeys.Add(item);
      }
      return lkeys;
    }

    // devuelve los tokens de un listado
    public List<ITokenInfo> GetTokens(string category)
    {

      if (dic.ContainsKey(category))
      {
        return dic[category];
      }
      else
      {
        return null;
      }
    }

    public void SortTokens(List<ITokenInfo> ltoken)
    {
      foreach (ITokenInfo item in ltoken)
      {
        // extraemos la clave
        string cat = item.Category;

        // si es clave nueva inicializamos el listado
        if (!dic.ContainsKey(cat))
        {
          List<ITokenInfo> laux = new List<ITokenInfo>();
          dic.Add(cat, laux);
        }

        // añadimos el elemento
        dic[cat].Add(item);
      }
    }
  }
}