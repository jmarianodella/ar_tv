using System;
using System.Collections.Generic;

namespace JMD.Token
{
  [Serializable]
  public class TokenInfo : ITokenInfo
  {
    public string Id
    {
      get { return _Id; }
      set { _Id = value; }
    }

    public DateTime Since
    {
      get { return _Since; }
      set { _Since = value; }
    }
    public DateTime Until
    {
      get { return _Until; }
      set { _Until = value; }
    }
    public string Reference
    {
      get { return _Reference; }
      set { _Reference = value; }
    }
    public List<string> Title
    {
      get { return _Title; }
      set { _Title = value; }
    }
    public List<string> Subtitle
    {
      get { return _Subtitle;}
      set { _Subtitle = value; }
    }
    public List<string> Video
    {
      get { return _Video; }
      set { _Video = value; }
    }
    public string Icon
    {
      get { return _Icon; }
      set { _Icon = value; }
    }
    public string Category
    {
      get { return _Category; }
      set { _Category = value; }
    }

    // private components

    private string _Id;
    private DateTime _Since;
    private DateTime _Until;
    private string _Reference;
    private List<string> _Title;
    private List<string> _Subtitle;
    private List<string> _Video;
    private string _Icon;
    private string _Category;


  }

}