using System.Collections.Generic;
using JMD.Token;

namespace JMD.Token.Animator
{
  public class TokenCarousellElement : ITokenCarousellElement
  {
    public eTokenAnimation type
    {
      get
      {
        return _type;
      }

      set
      {
        _type = value;
      }
    }

    public float time
    {
      get { return _time; }
      set { _time = value; }
    }
    public string category
    {
      get { return _category; }
      set { _category = value; }
    }

    public List<List<ITokenAnimator>> lTokenAnimator
    {
      get { return _lTokenAnimator; }
      set { _lTokenAnimator = value; }
    }

    // private components
    eTokenAnimation _type;
    float _time;
    string _category;
    List<List<ITokenAnimator>> _lTokenAnimator;
  }
}