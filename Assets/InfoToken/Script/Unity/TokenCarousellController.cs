﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JMD.DataBase;

using JMD.Token;
using JMD.Token.Animator;
using JMD.Token.Controller;
using JMD.Token.Database;
using System;

public class TokenCarousellController : MonoBehaviour, ITokenCarousellController
{

  // Use this for initialization
  ITokenController tokenController;

  // llistat de tokens
  List<ITokenInfo> ltokeninfo = new List<ITokenInfo>();

  // llistat d'animacions
  List<ITokenAnimator> lTokenAnimator = new List<ITokenAnimator>();

  // llistat de categories
  List<string> lcategories = new List<string>();

  DatabaseConnector database = new DatabaseConnector();


  // llistat d'animacions (capturades d'arxiu de configuració...)
  List<ITokenCarousellElement> lAnimation = new List<ITokenCarousellElement>();
  int indexLevel0Animation = 0; // Index del itoken animation en curso
  int indexLevel1Animation = 0; // index de la lista de animaciones del token animation en curso
  float timeAnimation = 0; // temps de l'animació en curs

  void Start()
  {
    // CarousellStart();
  }

  // Update is called once per frame
  void Update()
  {
    if (timeAnimation <= 0)
    {
      CarousellNext();
    }
    else
    {
      timeAnimation -= Time.deltaTime;
    }

  }
  // inicia el carousell de tokens
  public void CarousellInitTokens(ITokenController tokenController)
  {
    this.tokenController = tokenController;
    // recuperamos todas la categorías
    lcategories = tokenController.GetKeys();

    lAnimation = database.getTokenAnimations();


    // para cada sección definida en el archivo de animaciones aplicamos su animación
    foreach (TokenCarousellElement item in lAnimation)
    {
      // si para la animación definida en el listado de animaciónes
      // existen tokens definidos de la misma categoría...
      if (lcategories.Contains(item.category))
      {
        // TODO: lo siguiente:
        // buscar en el controlador los tokens de la categoria
        List<ITokenInfo> ltokenCategory = tokenController.GetTokens(item.category);
        // buscar los gameobject con el id del token
        // recuperar el componente de animación de cada gameobject 
        // guardarlo en el listado de animaciones del item actual 
        switch (item.type)
        {
          case eTokenAnimation.CATEGORY:
            // hay que poner todos los tokens en una sóla lísta
            item.lTokenAnimator = getTokenAnimatorByCategory(ltokenCategory);
            break;
          case eTokenAnimation.ICON:
            // hay que agrupar los tokens en listas según su icono
            item.lTokenAnimator = getTokenAnimatorByIcon(ltokenCategory);
            break;
          case eTokenAnimation.ID:
            // cada token es una lista para mostrarlos individualmente
            item.lTokenAnimator = getTokenAnimatorById(ltokenCategory);
            break;
          default:
            break;
        }
      }
    }
    Debug.Log("FIn init carousell controller");

  }
  public void CarousellStart()
  {
    // para cada elemento del carousel
    foreach (ITokenCarousellElement item in lAnimation)
    {
      if (item.lTokenAnimator == null) continue;
      // para cada grupo de animaciones contenida en cada elemento
      foreach (List<ITokenAnimator> ltoken in item.lTokenAnimator)
      {
        // para cada elemento del grupo
        foreach (ITokenAnimator token in ltoken)
        {
          token.InitTokenAnimation();
        }
      }
    }

    // preparamos indices
    indexLevel0Animation = 0;
    indexLevel1Animation = -1;
    CarousellNext();
  }


  public void CarousellgetOrder()
  {

  }

  public void CarousellNext()
  {
    Debug.Log("inicando siguiente grupo de animaciones");
    if (lAnimation == null || lAnimation.Count == 0)
    {
      // no hay animaciones a realizar
      Debug.Log("no hay animaciones a realizar");
      return;
    }

    List<List<ITokenAnimator>> currentLevel1Animation = lAnimation[indexLevel0Animation].lTokenAnimator;

    try
    {

      // paramos las animaciones actuales
      foreach (ITokenAnimator item in currentLevel1Animation[indexLevel1Animation])
      {
        item.TokenAwake(false);
      }
    }
    catch (Exception e)
    {
      Debug.LogWarning(e);
    }

    // actualizamos los indices
    CarousellUpdateIndex();

    // volvemos a iniciar el conteo de la animación
    timeAnimation = lAnimation[indexLevel0Animation].time;

    // iniciamos las animaciones nuevas
    foreach (ITokenAnimator item in currentLevel1Animation[indexLevel1Animation])
    {
      item.TokenAwake(true);
    }
  }

  private void CarousellUpdateIndex()
  {
    // indexLevel0Animation
    //  - indexLevel1Animation
    List<List<ITokenAnimator>> currentLevel1Animation = lAnimation[indexLevel0Animation].lTokenAnimator;

    if (indexLevel1Animation + 1 >= currentLevel1Animation.Count)
    {
      // en caso afirmativo incrementamos el nivel 0 hasta que encontremos un nivel con animaciones
      // y volvemos a llamar a carousellNext
      do
      {
        indexLevel0Animation = (indexLevel0Animation + 1) % lAnimation.Count;
        currentLevel1Animation = lAnimation[indexLevel0Animation].lTokenAnimator;
      } while (currentLevel1Animation == null);
      // reseteamos el contador de level 1
      indexLevel1Animation = 0;
    }
    else
    {
      // pasamos al siguiente grupo de animaciones de level 1
      indexLevel1Animation = (indexLevel1Animation + 1) % currentLevel1Animation.Count;
    }
  }

  private List<List<ITokenAnimator>> getTokenAnimatorById(List<ITokenInfo> ltokenCategory)
  {
    List<List<ITokenAnimator>> result = new List<List<ITokenAnimator>>();

    foreach (ITokenInfo item in ltokenCategory)
    {
      ITokenAnimator animator = recoverITokenAnimatorByID(item.Id);
      result.Add(new List<ITokenAnimator>() { animator });
    }

    return result;
  }

  private List<List<ITokenAnimator>> getTokenAnimatorByIcon(List<ITokenInfo> ltokenCategory)
  {
    Dictionary<string, List<ITokenAnimator>> dic = new Dictionary<string, List<ITokenAnimator>>();
    List<List<ITokenAnimator>> result = new List<List<ITokenAnimator>>();

    // recuperamos los tokenAnimator y los agrupamos por icono
    foreach (ITokenInfo item in ltokenCategory)
    {
      ITokenAnimator animator = recoverITokenAnimatorByID(item.Id);

      if (!dic.ContainsKey(item.Icon)) dic[item.Icon] = new List<ITokenAnimator>();
      dic[item.Icon].Add(animator);
    }

    // por cada clave contenida en el diccionario extraemos el listado y lo devolemos como resultado
    foreach (string index in dic.Keys)
    {
      result.Add(dic[index]);
    }

    return result;
  }

  private List<List<ITokenAnimator>> getTokenAnimatorByCategory(List<ITokenInfo> ltokenCategory)
  {
    List<List<ITokenAnimator>> result = new List<List<ITokenAnimator>>();

    List<ITokenAnimator> ltokenanimator = new List<ITokenAnimator>();
    foreach (ITokenInfo item in ltokenCategory)
    {
      ITokenAnimator tkAnimator = recoverITokenAnimatorByID(item.Id);
      if (tkAnimator != null)
      {
        ltokenanimator.Add(tkAnimator);
      }
      else
      {
        Debug.LogError("el token con id " + item.Id + " no tiene token animator");
      }

    }

    result.Add(ltokenanimator);
    return result;
  }

  private ITokenAnimator recoverITokenAnimatorByID(string id)
  {
    GameObject obj = tokenController.GetGameObject(id);
    // gameObject.transform.Find(id).gameObject;
    if (!obj) return null;

    return obj.GetComponent<ITokenAnimator>();
  }


  // inicia las animaciones de una lista
  private void CarousellInitAnimations(List<ITokenAnimator> lToken)
  {
    // paramos los tokens actuales
    foreach (ITokenAnimator anim in lTokenAnimator)
    {
      anim.TokenAwake(false);
    }
    // limpiamos listado
    lTokenAnimator.Clear();
    // añadimos nueva lista
    lTokenAnimator.AddRange(lToken);

    // iniciamos listado
    foreach (ITokenAnimator item in lTokenAnimator)
    {
      // para cada uno inicializamos la animación
      item.InitTokenAnimation();
    }


  }

}
