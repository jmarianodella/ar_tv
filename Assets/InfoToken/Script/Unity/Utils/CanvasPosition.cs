﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasPosition : MonoBehaviour {

	// Use this for initialization
	
	public Vector3 pos3D;
		void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Camera cam = Camera.main;
		pos3D =  cam.WorldToScreenPoint(transform.parent.position);
		transform.Find("Text").position = pos3D;
	}
}
