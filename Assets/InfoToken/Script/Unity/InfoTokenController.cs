﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

using UnityEditor;

using TMPro;

using JMD.Token;
using JMD.Token.Animator;

// muestra la información de un token
public class InfoTokenController : MonoBehaviour, ITokenAnimator
{

  // Object reference
  public GameObject reference;
  public List<string> title, subtitle, video;
  public string icon;
  public ITokenInfo token
  {
    set
    {
      // asignamos los valores el elemento
      title = value.Title;
      subtitle = value.Subtitle;
      video = value.Video;
      icon = value.Icon;
      // checkTag(value.Category);
      // gameObject.tag = value.Category;

      reference = GameObject.Find(value.Reference);
    }
  }

  public bool activate
  {
    get { return activate; }
    set
    {
      _activate = value;
      if (_activate)
      {
        TokenAwake(true);
      }
      else
      {
        TokenAwake(false);
      }
    }
  }

  public Status status = Status.IDLE;

  private Animator iconAnimator;
  private Animator canvasAnimator;
  private bool _activate = false;
  private GameObject player, goVideoContainer;
  private VideoPlayer videoPlayer;
  private int indexTitle = 0, indexVideo = 0;

  public enum Status
  {
    IDLE, ROTATION, TOIDLE, SLEEP
  }


  void Start()
  {



    // iniciamos la animación del token
    InitTokenAnimation();

    // asignamos los valores iniciales
    setInfo();

    // asignamos a reference como padre
    transform.parent = reference.transform;
    transform.position = reference.transform.position;
  }

  // Update is called once per frame
  void Update()
  {
    // ponemos el token en la posición del objeto de referencia
    // transform.position = reference.transform.position;

    if (Input.GetKeyDown(KeyCode.Space))
    {
      Debug.Log("space pressed");
      iconAnimator.SetBool("awake", !iconAnimator.GetBool("awake"));

    }
  }

  /**********  funciones públicas **********/
  public void ActivateInfo()
  {
    activate = !activate;
  }

  public void nextText()
  {
    // incrementamos el campo de cada uno de los elementos
    // los campos titulo y subtítulo deben ir a la par
    indexTitle = (indexTitle + 1) % title.Count;

    // asignamos título
    setText("Canvas/Text/Title", title[indexTitle]);

    // asignamos subtítulo
    setText("Canvas/Text/Subtitle", subtitle[indexTitle]);

  }

  // reproduce el vídeo siguiente
  public void nextVideo()
  {
    indexVideo = (indexVideo + 1) % video.Count;

    // asignamos vídeo
    setVideo("Canvas/Text/VideoContainer/VideoPlayer", video[indexVideo]);
  }

  /**********  funciones privadas **********/

  // asignación inicial de textos, icono y videos
  private void setInfo()
  {

    // asignamos título
    if (title != null && title.Count > 0)
      setText("Canvas/Text/Title", title[indexTitle]);
    // asignamos subtítulo
    if (subtitle != null && subtitle.Count > 0)
      setText("Canvas/Text/Subtitle", subtitle[indexTitle]);
    // asignamos vídeo
    if (video != null && video.Count > 0)
    {
      setVideo("Canvas/Text/VideoContainer/VideoPlayer", Application.dataPath + "/" + video[indexVideo]);
    }
    // asignamos icono
    setIcon("Asset/Icon/Mesh", icon);
  }
  void setText(string target, string txt)
  {
    if (txt.Length == 0)
    {

      Transform obj = gameObject.transform.Find(target);
      Debug.Log("object name " + obj.name);
      obj.gameObject.SetActive(false);
    }
    else
    {
      TMP_Text text = gameObject.transform.Find(target).gameObject.GetComponent<TMP_Text>();
      text.text = txt;
    }
  }

  void setVideo(string target, string path)
  {
    if (path.Length != 0)
    {
      // seleccionar los elementos
      player = gameObject.transform.Find(target).gameObject;
      videoPlayer = player.GetComponent<VideoPlayer>();
      RenderTexture texture = new RenderTexture(1920, 1080, 16, RenderTextureFormat.ARGB32);

      // preparamos el vídeo
      videoPlayer.playOnAwake = false;
      videoPlayer.url = path;

      // ocultamos el contenedor hasta nueva orden
      goVideoContainer.SetActive(false);

      // asignamos la textura al visor
      player.GetComponent<RawImage>().texture = texture;
      videoPlayer.targetTexture = texture;

    }
  }

  void setIcon(string target, string txt)
  {
    if (txt == null)
    {
      Transform obj = gameObject.transform.Find(target);
      obj.gameObject.SetActive(false);
      return;
    }
    Debug.Log("icon to Load " + txt);
    if (txt.Length == 0)
    {
      // si no hay icono asignado desactivamos la geometría
      Transform obj = gameObject.transform.Find(target);
      obj.gameObject.SetActive(false);
    }
    else
    {
      // cargamos la textura
      Texture texture = Resources.Load("InfoToken/Textures/" + txt) as Texture;

      // asignamos a ambos planos
      Renderer mat = gameObject.transform.Find(target + "/Plane").gameObject.GetComponent<Renderer>();
      mat.material.SetTexture("_MainTex", texture);
      mat = gameObject.transform.Find(target + "/PlaneBck").gameObject.GetComponent<Renderer>();
      mat.material.SetTexture("_MainTex", texture);
    }
  }


  private void checkTag(string tagname)
  {
    // Open tag manager
    SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
    SerializedProperty tagsProp = tagManager.FindProperty("tags");

    // For Unity 5 we need this too
    SerializedProperty layersProp = tagManager.FindProperty("layers");

    // Adding a Tag
    string s = tagname;

    // First check if it is not already present
    bool found = false;
    for (int i = 0; i < tagsProp.arraySize; i++)
    {
      SerializedProperty t = tagsProp.GetArrayElementAtIndex(i);
      if (t.stringValue.Equals(s)) { found = true; break; }
    }

    // if not found, add it
    if (!found)
    {
      tagsProp.InsertArrayElementAtIndex(0);
      SerializedProperty n = tagsProp.GetArrayElementAtIndex(0);
      n.stringValue = s;
    }

  }

  // implementación de interface de animación //
  public void InitTokenAnimation()
  {
    // capturamos el animator del icono
    iconAnimator = gameObject.transform.Find("Asset/Icon").GetComponent<Animator>();
    canvasAnimator = gameObject.transform.Find("Canvas").GetComponent<Animator>();
    // Debug.Log("iniciando token: " + gameObject.name);
    // capturamos el contenedor
    goVideoContainer = gameObject.transform.Find("Canvas/Text/VideoContainer").gameObject;

    // lo marcamos como no activo
    activate = false;
  }

  public void TokenAwake(bool awake)
  {
    // si hay que dormir paramos el vídeo
    // gameObject.SetActive(awake);
    if (this.video != null && this.video.Count > 0)
    {

      goVideoContainer.SetActive(awake);
      TokenPlayVideo(awake);
      // if (!awake)
      // {
      //   TokenSelected(false);
      // }
    }
    if (iconAnimator) iconAnimator.SetBool("awake", awake);
    if (canvasAnimator) canvasAnimator.SetBool("awake", awake);


  }

  public void TokenPlayVideo(bool play)
  {
    if (video == null) return;
    if (videoPlayer == null) return;

    if (video.Count > 0 && play)
    { videoPlayer.Play(); }
    else
    { videoPlayer.Stop(); }
  }

  public void TokenSelected(bool selected)
  {
  }
}
