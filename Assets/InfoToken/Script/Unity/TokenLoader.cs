﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JMD.Token;
using JMD.Token.Animator;
using JMD.Token.Controller;
using JMD.Token.Database;
using JMD.DataBase;

// carga los tokens
public class TokenLoader : MonoBehaviour
{

  // 
  DatabaseConnector database = new DatabaseConnector();
  public ITokenController tokenController = new TokenController();
  // Use this for initialization
  GameObject tokenInfo;
  void Start()
  {
    // preparamos las animaciones
    gameObject.GetComponent<ITokenCarousellController>().CarousellInitTokens(tokenController);
    gameObject.GetComponent<ITokenCarousellController>().CarousellStart();


  }

  void Awake()
  {
    // instanciamos el token
    tokenInfo = Resources.Load("InfoToken/Prefab/InfoToken") as GameObject;

    // recuperamos los tokens
    tokenController.SortTokens(database.getTokens(new List<ITokenInfo>()));

    foreach (string category in tokenController.GetKeys())
    {
      deployTokens(category, tokenController.GetTokens(category));

    }

  }

  // Update is called once per frame
  void Update()
  {

  }

  // genera un objeto token para cada elemento del listado
  private void deployTokens(string category, List<ITokenInfo> ltokens)
  {
    foreach (ITokenInfo item in ltokens)
    {
      Debug.Log(item.Id);
      GameObject tk = Instantiate(tokenInfo);
      tokenController.AddGameObject(item.Id, tk);

      tk.name = item.Id;
      tk.transform.parent = transform;
      tk.GetComponent<InfoTokenController>().token = item;
    }
  }
}
