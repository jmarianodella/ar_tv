﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class timer : MonoBehaviour {

	TMP_Text text;
	// Use this for initialization
	void Start () {
		text = gameObject.GetComponent<TMP_Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if (text != null){
			text.text = Time.time.ToString();
		}
	}
}
