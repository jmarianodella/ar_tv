namespace JMD.Entities
{
  public class Video : IVideo
  {
    string _assetid;
    public string assetid
    {
      get
      {
        return _assetid;
      }

      set
      {
        _assetid = value;
      }
    }

    private string _path;
    public string path
    {
      get
      {
        return _path;
      }
      set
      {
        this._path = value;
      }
    }
  }
}