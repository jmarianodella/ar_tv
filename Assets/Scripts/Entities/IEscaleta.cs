﻿// using System.Collections;
using System.Collections.Generic;

namespace JMD.Entities
{

  public interface IEscaleta
  {
    // identificador de escaleta
    string id
    {
      get; set;
    }

    // nombre asocido a la escaleta
    string name
    {
      get; set;
    }

    //Lista de linees d'escaleta
    List<ILine> line
    {
      get; set;
    }

		// acciones sobre escaleta

		// carga la escaleta de la base de datos
		void load(string id);
  }

}