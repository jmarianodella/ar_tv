using System.Collections.Generic;

namespace JMD.Entities
{
  public class Escaleta : IEscaleta
  {
    public string id
    {
      get { return _id; }
      set { _id = value; }
    }

    public string name
    {
      get { return _name; }
      set { _name = value; }
    }
    public List<ILine> line
    {
      get { return _line; }
      set { _line = value; }
    }

    public void load(string id)
    {
      Escaleta esc = loadDebugEscaleta();
      this.id = id;
      this.name = esc.name;
      this.line = esc.line;
    }

    // variables privadas
    private string _id;
    private string _name;
    private List<ILine> _line;
    private Escaleta loadDebugEscaleta()
    {
      Escaleta esc = new Escaleta();
      esc.name = "Debug escaleta";
      esc.line = loadDebugLines();
      return esc;
    }

    private List<ILine> loadDebugLines()
    {
      List<ILine> l = new List<ILine>();

      for (int i = 0; i < 10; i++)
      {
        ILine linea = new Line();
        linea.id = "10" + i;
        linea.txt = "texto debug " + i;
        linea.chyron = loadDebugChyron(i);
        l.Add(linea);

      }

      return l;
    }

    private List<IChyron> loadDebugChyron(int i)
    {
      List<IChyron> lchy = new List<IChyron>();

      IChyron chyron = new Chyron();
      chyron.id = "AR_DEBUG_" + i;
      chyron.tcin = 0;
      chyron.tcout = -1;
      chyron.text = new List<string> { "hola", "adios" };

      lchy.Add(chyron);

      return lchy;
    }
  }

}