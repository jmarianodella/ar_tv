using System.Collections.Generic;

namespace JMD.Entities
{
  public class Chyron : IChyron
  {
    public string id
    {
      get { return _id; }
      set { _id = value; }
    }

    public List<string> text
    {
      get { return _text; }
      set { _text = value; }
    }

    public float tcin
    {
      get { return _tcin; }
      set { _tcin = value; }
    }

    public float tcout
    {
      get { return _tcout; }
      set { _tcout = value; }
    }
    
    // variables privadas
    private string _id;
    private float _tcin;
    private float _tcout;
    private List<string> _text;
  }

}