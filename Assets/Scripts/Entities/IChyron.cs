﻿// using System.Collections;
using System.Collections.Generic;

namespace JMD.Entities
{

  public interface IChyron
  {
    // identificador del chyron
    string id
    {
      get; set;
    }

    // texo asociado
    List<string> text{
      get; set;
    }

    // tc entrada desde que se da al play
    float tcin
    {
      get; set;
    }
    
    // tc salida
    float tcout{
      get; set;
    }

  }

}