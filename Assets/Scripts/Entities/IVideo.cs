﻿// using System.Collections;
using System.Collections.Generic;

namespace JMD.Entities
{

  public interface IVideo
  {
    // identificador de escaleta
    string assetid
    {
      get; set;
    }
    // nombre asocido a la escaleta
    string path
    {
      get; set;
    }

  }

}