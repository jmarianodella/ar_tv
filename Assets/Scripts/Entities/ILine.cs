using System.Collections.Generic;

namespace JMD.Entities
{
  public interface ILine
  {
    // id de la línea
    string id
    {
      get; set;
    }

    // texto asociado a la línea
    string txt
    {
      get; set;
    }

    // chyrons asociados
    List<IChyron> chyron{
      get; set;
    }
  }
}