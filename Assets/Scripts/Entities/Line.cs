using System.Collections.Generic;

namespace JMD.Entities
{
  public class Line : ILine
  {
    public string id
    {
      get { return _id; }
      set { _id = value; }
    }

    public string txt
    {
      get { return _txt; }
      set { _txt = value; }
    }
    public List<IChyron> chyron
    {
      get { return _chyron; }
      set { _chyron = value; }
    }

    // variables privadas
    private string _id;
    private string _txt;
    private List<IChyron> _chyron;
  }
}