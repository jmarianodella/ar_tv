﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JMD.Entities;

namespace JMD.Game
{

  public class LineaController : MonoBehaviour, ILineaController
  {

    public ILine line
    {
      set
      {
        _line = value;
        InitAR();
      }
      get
      {
        return _line;
      }
    }
    public ILine _line;
    // private value
    private List<GameObject> lAR = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

      KeysManagement();
    }

    public void InitAR()
    {
			foreach (GameObject item in lAR)
			{
				Destroy(item);
			}
			lAR.Clear();
      foreach (IChyron item in line.chyron)
      {
        InitChyron(item);
      }
    }

    public void InitChyron(IChyron ch)
    {
      string resourceName = ch.id;
      GameObject obj = Resources.Load(resourceName) as GameObject;
      GameObject ins = Instantiate(obj);
      ins.transform.parent = transform;
      ins.SetActive(false);

      lAR.Add(ins);

      Debug.Log("resource name " + resourceName);
    }

    public void Play()
    {
      if (lAR.Count > 0)
      {
        if (lAR[0].activeSelf)
        {
          // desactivar elementos
          //TODO: animación de salida ¿?
          foreach (GameObject item in lAR)
          {
            item.SetActive(!item.activeSelf);
          }
        }
        else
        {
          // activar elementos
          //TODO: animación de entrada ¿?
          foreach (GameObject item in lAR)
          {
            item.SetActive(!item.activeSelf);
          }

        }
      }
    }
    void KeysManagement()
    {
      if (Input.GetKeyDown(KeyCode.F5) ||
      Input.GetKeyDown(KeyCode.Space))
      {
        // play elements
        Debug.Log("play asset");
        Play();
      }
    }
  }


}