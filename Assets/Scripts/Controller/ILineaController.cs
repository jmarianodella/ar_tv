using JMD.Entities;

namespace JMD.Game
{
  public interface ILineaController
  {
    // contiene la información de la línea
    ILine line { get; set; }

    // inicia las geometrías de AR
    void InitAR();
    // genera la geometría asociada a un chyron
    void InitChyron(IChyron ch);
    // inicia las animaciones de las geometrías
    void Play();
  }

}