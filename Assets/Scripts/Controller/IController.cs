using JMD.Entities;
namespace JMD.Game
{
    public interface IController
    {
        IEscaleta currentEscaleta{
          get; set;
        }

        // pasamos a la siguiente línea
        void AvLine();
        // pasamos a la anterior línea
        void ReLine();
        // inicia reproducció de la escaleta
        void PlayEscaleta();
        // cambia entre calibración y online
        void ToggleMode();

        void ShowLineInfo();
        void SetLine();
    }
}