﻿using System.Collections;
using System.Collections.Generic;
using JMD.DataBase;
using JMD.Entities;
using UnityEngine;
using TMPro;

namespace JMD.Game
{

  public class Controller : MonoBehaviour, IController
  {
    public STATUS status;
    public string escaleta;
    public string currentLine;


    public enum STATUS
    {
      MENU,
      CALIBRATING,
      ONLINE,
    }

    public IEscaleta currentEscaleta
    {
      get { return _escaleta; }
      set
      {
        _escaleta = value;
        escaleta = _escaleta.name;
      }
    }

    public int contador;

    // private vars
    private IEscaleta _escaleta;
    private int line = 0;
    private IDataBase _db;
    public GameObject lineaController;
    private GameObject canvas;
    private TMP_Text line_name;

    private int numLineasEscaleta;

    // Use this for initialization
    void Start()
    {
      // iniciamos la base de datos
      _db = new DataBase.DataBase();
      // cargamos la escaleta de debug 1
      currentEscaleta = _db.GetEscaleta("1");
      numLineasEscaleta = currentEscaleta.line.Count;

      // capuramos el panel
      canvas = gameObject.transform.Find("Canvas").gameObject;
      line_name = canvas.transform.Find("TextInfo").GetComponent<TMP_Text>();
      Debug.Log(currentEscaleta.id + "- " + currentEscaleta.name);
      
      // asignamos la línea inicial al LineaController
      SetLine();
    }

    // Update is called once per frame
    void Update()
    {
      KeysManagement();

      ShowLineInfo();
    }

    public void AvLine()
    {
      line = (line + 1) % numLineasEscaleta;
      SetLine();
    }

    public void ReLine()
    {
      line = (line - 1) % numLineasEscaleta;
      SetLine();
    }

    public void PlayEscaleta()
    {
      lineaController.SetActive(true);
    }

    public void ToggleMode()
    {
      switch (status)
      {
        case STATUS.CALIBRATING: status = STATUS.ONLINE; break;
        case STATUS.ONLINE: status = STATUS.CALIBRATING; break;
        case STATUS.MENU: break;
        default: break;

      }
    }

    void KeysManagement(){
      if (Input.GetKeyDown(KeyCode.PageDown)){
        AvLine();
      }
      if (Input.GetKeyDown(KeyCode.PageUp)){
        ReLine();
      }
        if (Input.GetKeyDown(KeyCode.Period)){
        ToggleMode();
      }
      
    }

    public void ShowLineInfo()
    {
      if (status == STATUS.CALIBRATING){
        canvas.SetActive(true);
      }else{
        canvas.SetActive(false); 

      }
    }

    public void SetLine(){
      lineaController.GetComponent<LineaController>().line = currentEscaleta.line[line];
      currentLine = currentEscaleta.line[line].id;
      line_name.text = currentLine;
    }
  }

}