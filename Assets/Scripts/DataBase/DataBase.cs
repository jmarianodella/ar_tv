using System.Collections.Generic;
using JMD.Entities;

namespace JMD.DataBase
{
  public class DataBase : IDataBase
  {
    public IEscaleta GetEscaleta(string id)
    {
      IEscaleta esc = new Escaleta();
      esc.load(id);
      return esc;
    }

    public List<string> GetEscaletas()
    {
      return new List<string> { "MIGDIA", "VESPRE", "MATINS", "5DIES" };
    }

    public void Init()
    {
      // de momento nada
    }
  }
}