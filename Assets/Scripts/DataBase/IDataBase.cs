﻿// using System.Collections;
using System.Collections.Generic;

using JMD.Entities;

namespace JMD.DataBase
{

  public interface IDataBase
  {
    // inicializa la conexión
    void Init();

    // recupera el listado de escaletas disponible
    List<string> GetEscaletas();

    // recupera la escaleta solicitada
    IEscaleta GetEscaleta(string id);
    
  }

}