namespace JMD.AR.Asset
{
  interface IARAsset
  {
    // identificador del asset
    string id
    {
      get; set;
    }

    // inicia la animación
    void Play();

    // para la animación
    void Pause();

    // hace desaparecer el objeto
    void Destroy();
  }

}