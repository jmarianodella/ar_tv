﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace JMD.text1
{

  public class curvedText : MonoBehaviour
  {
    public string text;
    public GameObject tmp;

    public List<Transform> lLetters;

    [Range(0, 15f)]
    public float radius = 0;

    [Range(0, 360f)]
    public float maxAngle = 360f;

    [Range(0, 5f)]
    public float textSize = 0.4f;


    // Use this for initialization
    void Start()
    {
      tmp.SetActive(true);
      lLetters = new List<Transform>();
      char[] atext = text.ToCharArray();
      float angle = maxAngle / atext.Length;

      // lista de longitudes de los diferentes char
      List<float> charLength = new List<float>();
      float totalCharLength = 0;
      // para cada char del texto
      foreach (char item in atext)
      {
        // container que contiene el texto
        GameObject container = new GameObject();
        container.transform.parent = transform;
        container.transform.position = transform.position;
        container.transform.rotation = transform.rotation;

        container.name = item.ToString();
        lLetters.Add(container.transform);

        // text mesh
        GameObject l = Instantiate(tmp, Vector3.zero, container.transform.rotation, container.transform);
        l.transform.localPosition = new Vector3(0, 0, radius);
        l.transform.Rotate(0, 180, 0);

        // Asignamos el char
        TMP_Text txt = l.GetComponent<TMP_Text>();
        txt.text = item.ToString();

        // calculamos la longitud del texto
        Vector2 textSize = txt.GetPreferredValues(item.ToString());
        charLength.Add(textSize.x);
        totalCharLength += textSize.x;


      }

      //aplicamos los giros a cada caracter segun su longitud
      int count = 0;
      float accumulateAngle = 0;
      for (int i=0; i<charLength.Count; i++){
        float sin, cos;

        sin = charLength[i]/2;
        cos = radius;
        float rotateAngle = Mathf.Atan(sin/cos) * 2 * 2 * Mathf.PI;
        accumulateAngle -= rotateAngle;

        lLetters[i].Rotate(0, accumulateAngle, 0);
        count++;
      }      // aquí no...
      tmp.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
  }

}